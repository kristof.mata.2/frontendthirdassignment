## Name
Frontend Third assignment.

## Description
Pokémon Trainer web app using the Angular Framework, capable of managing the trainers personal collection and an option to check out all the avaliable pokémons.

## Image
Login page:
![image.png](./image.png)
Trainer page:
![image-1.png](./image-1.png)
Catalogue page:
![image-2.png](./image-2.png)


## Installation
Install NodeJS.
Run npm start from the root directory of the application folder.
Login page is accesible on localhost:4200
Or use the heroku deploy: https://evening-caverns-81452.herokuapp.com/catalogue

## Usage
Login with the login form.
Navigate with the navigation bar
In the catalogue menu:
  Add button - adds that pokemon to Trainer's collection.
  Ball - removes that pokemon from Trainer's collection.
  Navigate with page numbers at the bottom of the page.
In the Trainer menu:
  Ball - removes that pokemon from Trainer's collection.

## Heroku
https://evening-caverns-81452.herokuapp.com/
## Maintainers
@tothbt
@kristof.mata.2

## Project status
Development has slowed down or stopped completely.
