import { Sprite } from "./sprite.model";

export interface Pokemon {
    id: number;
    name: string;
    sprites: Sprite; 
}