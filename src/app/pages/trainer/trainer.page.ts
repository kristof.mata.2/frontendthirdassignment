import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { StrorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage{


  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }
  
  get pokemons(): Pokemon[] {
    if(this.trainerService.trainer){
      let owned: Pokemon[]=[];
      this.trainerService.trainer.pokemon.forEach((name) =>{
        if(StrorageUtil.storageReadByName<Pokemon>(name) === undefined){
          this.pokemonCatalogueService.findPokemonsByName(name)
          owned.push(this.pokemonCatalogueService.pokemon)
        }
        else{
          owned.push(<Pokemon>StrorageUtil.storageReadByName<Pokemon>(name))
        }
        
      });
      
      return owned
    }
    return [];
  }

  
  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ){ }
  


}
