import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit{

  constructor(
      private readonly router: Router,
      private readonly trainerService: TrainerService,
      private readonly pokemonCatalogueService: PokemonCatalogueService
    ) { }
  

  handleLogin(): void {
    this.router.navigateByUrl("/trainers")
  }

  ngOnInit(): void {
  }

}
