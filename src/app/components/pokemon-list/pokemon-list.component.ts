import { Component, Input, OnInit } from '@angular/core';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';


@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent{

  @Input() pokemons: any[] = []; 
  @Input() page: number = 1;

  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }

  public changePage(): void{
    this.pokemonCatalogueService.setPokemons = [];
    this.pokemonCatalogueService.findPageOfPokemons(12, (this.page-1)*12);
  }


}
