import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { AddService } from 'src/app/services/add.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.css']
})
export class AddButtonComponent implements OnInit {

  public loading: boolean = false;
  public isAdded: boolean = false;

  @Input() pokemonName: string = "";

  constructor(
    private readonly pokemonAddService: AddService,
    private readonly trainerService: TrainerService
  ) { }

  ngOnInit(): void {
    this.isAdded = this.trainerService.inPokemons(this.pokemonName);
  }

  onAddClick(): void {
    this.loading = true;
    this.pokemonAddService.AddToTrainer(this.pokemonName)
    .subscribe({
      next: (trainer : Trainer)  => {
        this.loading=false;
        this.isAdded = this.trainerService.inPokemons(this.pokemonName);
      },
      error: (error: HttpErrorResponse) => {
        console.log("Error:", error.message);
      }
    })
    
  }
}
