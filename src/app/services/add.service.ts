import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

const { apiKey, apiUsers } = environment

@Injectable({
  providedIn: 'root'
})
export class AddService {

  constructor(
    public readonly http: HttpClient,
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonCatalogueService
  ) { }

  public AddToTrainer(pokemonName: string) {
    const trainer: Trainer | undefined = this.trainerService.trainer;
    const pokemon: any | undefined = this.pokemonService.pokemonByName(pokemonName);

    if (!pokemon) {
      throw new Error("No pokemon with that name");
    }

    if (!trainer) {
      throw new Error("There is no trainer");
    }

    if (this.trainerService.inPokemons(pokemonName)) {
      console.log(pokemonName);
      this.trainerService.removePokemon(pokemonName);
    } else {
      console.log("not found");
      this.trainerService.addToPokemons(pokemonName);
    }

    const headers = new HttpHeaders({
      'content-Type': 'application/json',
      'x-api-key': apiKey
    })

    return this.http.patch<Trainer>(`${apiUsers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon] //already updated at this point
    }, { headers })
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer;
        })
      );
  }
}
