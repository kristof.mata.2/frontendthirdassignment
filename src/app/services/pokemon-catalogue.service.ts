import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { StrorageUtil } from '../utils/storage.util';


const { apiPokemons } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _pokemon: Pokemon={
    id: 0,
    name: "asd",
    sprites: {
      front_default: "asd"
    }
  };
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get pokemon(): Pokemon {
    return this._pokemon;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  set setPokemons(pokemons: any[]) {
    this._pokemons = pokemons;
  }

  constructor(
    private readonly http: HttpClient,
  ) { }


  public findPageOfPokemons(limit: number, offset: number): void {
    this.http.get<any>(`${apiPokemons}?limit=${limit}&offset=${offset}`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (pokemons: any) => {
          pokemons.results.forEach((result: { name: any; }) => {
            if (StrorageUtil.storageReadByName<Pokemon>(result.name) === undefined) {
              this.http.get<any>(`${apiPokemons}/${result.name}`)
                .subscribe((uniqueResponse: Pokemon) => {
                  let newPokemon: Pokemon = {
                    id: uniqueResponse.id,
                    name: uniqueResponse.name,
                    sprites: uniqueResponse.sprites
                  }
                  this._pokemons.push(newPokemon);
                  StrorageUtil.storageSave<Pokemon>(newPokemon.name, newPokemon)
                })
            }
            else{
              this._pokemons.push(<Pokemon>StrorageUtil.storageReadByName<Pokemon>(result.name))
            }
          })
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }

  public findPokemonsByName(pokemonName: string): void {
    let newPokemon: Pokemon
    this.http.get<any>(`${apiPokemons}/${pokemonName}`)
                .subscribe((uniqueResponse: Pokemon) => {
                  newPokemon= {
                    id: uniqueResponse.id,
                    name: uniqueResponse.name,
                    sprites: uniqueResponse.sprites
                  }
                  this._pokemon=newPokemon;
                  StrorageUtil.storageSave<Pokemon>(newPokemon.name, newPokemon)
                })
  }


  
  
  public pokemonByName(name: string): any | undefined {
    
    return <Pokemon>StrorageUtil.storageReadByName<Pokemon>(name)
  }
}
