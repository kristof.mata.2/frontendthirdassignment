import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { StorageKeys } from '../enums/storage-keys.enum';
import { Trainer } from '../models/trainer.model';
//import { StrorageUtil } from '../utils/storage.util';

const { apiUsers, apiKey }=environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //Dependency Injection.
  constructor(private readonly http: HttpClient) { }

  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username)
      .pipe(
        switchMap((trainer: Trainer | undefined) => {
          if(trainer === undefined){//trainer does not exist
            return this.createTrainer(username);
          }
          return of(trainer);
        })/*,
        tap((trainer: Trainer) => {
          StrorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer)
        })*/
      )
  }

  //check if user exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiUsers}?username=${username}`)
      .pipe(//RxJS
        map((response: Trainer[]) => response.pop())
      )
  }

  private createTrainer(username: string): Observable<Trainer> {
    //trainer
    const trainer = {
      username,
      pokemon: []
    };
    //header -> API Key
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })
    //POST - Create items on the server
    return this.http.post<Trainer>(apiUsers, trainer, {
      headers
    })

  }

}
