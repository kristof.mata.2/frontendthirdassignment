import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StrorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StrorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!)
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StrorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  public inPokemons(pokemonName: string): boolean {
    if (this._trainer) {
      return Boolean(this._trainer?.pokemon.find((pokemons: string) => pokemons === pokemonName));
    }
    return false;
  }

  public addToPokemons(pokemonName: any): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemonName);
    }
  }

  public removePokemon(pokemonName: string): void {
    if (this._trainer) {
      const index = this._trainer.pokemon.indexOf(pokemonName);
      if (index > -1) {
        this._trainer.pokemon.splice(index, 1);
      }
    }
  }


}
